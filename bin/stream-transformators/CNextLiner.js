const { Transform } = require('stream');
const { StringDecoder } = require('string_decoder');

module.exports = class CNextLiner extends Transform {
  constructor() {
    super();

    this.stringDecoder = new StringDecoder('utf-8');
  }

  // eslint-disable-next-line no-underscore-dangle
  _transform(chunk, encoding, callback) {
    let chunkString = '';
    if (encoding === 'buffer') {
      chunkString = this.stringDecoder.write(chunk);
    }

    callback(null, `${chunkString}\r\n`);
  }
};
