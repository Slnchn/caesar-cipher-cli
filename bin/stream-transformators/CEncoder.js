const { Transform } = require('stream');
const { StringDecoder } = require('string_decoder');

const caesarEncoder = require('../caesar-encoder');

const { scramblerActions } = require('../constants');

module.exports = class CEncoder extends Transform {
  constructor(shift, action) {
    super();

    this.stringDecoder = new StringDecoder('utf-8');
    this.shift = action === scramblerActions.ENCODE ? shift : -shift;
  }

  // eslint-disable-next-line no-underscore-dangle
  _transform(chunk, encoding, callback) {
    let chunkString = '';
    if (encoding === 'buffer') {
      chunkString = this.stringDecoder.write(chunk);
    }

    callback(null, caesarEncoder(chunkString, this.shift));
  }
};
