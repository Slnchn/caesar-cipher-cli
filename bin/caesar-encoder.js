function normalizeShift(initialShift) {
  let normalizedShift = initialShift;
  if (normalizedShift < 0) {
    while (normalizedShift < -26) {
      normalizedShift += 26;
    }

    normalizedShift += 26;
  } else {
    while (normalizedShift > 26) {
      normalizedShift -= 26;
    }
  }

  return normalizedShift;
}

module.exports = (initialText, initialShift) => {
  let output = '';
  const shift = normalizeShift(initialShift);
  for (let i = 0; i < initialText.length; i += 1) {
    let char = initialText[i];
    if (char.match(/[a-z]/i)) {
      const code = initialText.charCodeAt(i);
      if (code >= 65 && code <= 90) {
        char = String.fromCharCode(((code - 65 + shift) % 26) + 65);
      } else if (code >= 97 && code <= 122) {
        char = String.fromCharCode(((code - 97 + shift) % 26) + 97);
      }
    }

    output += char;
  }

  return output;
};
