const fs = require('fs');

const scramblerActions = {
  ENCODE: 'encode',
  DECODE: 'decode',
};

const streamConfigs = {
  READ_STREAM: {
    std: process.stdin,
    creator: fs.createReadStream,
    fileAccessChecks: [
      {
        mode: fs.constants.F_OK,
        errorHandler: () => Promise.reject(new Error('Noticed input file is not exist.')),
      },
      {
        mode: fs.constants.R_OK,
        errorHandler: () => Promise.reject(new Error('Noticed input file is not readable')),
      },
    ],
  },
  WRITE_STREAM: {
    std: process.stdout,
    creator: fs.createWriteStream,
    options: { flags: 'a' },
    fileAccessChecks: [
      {
        mode: fs.constants.F_OK,
        errorHandler: () => Promise.reject(new Error('Noticed output file is not exist.')),
      },
      {
        mode: fs.constants.W_OK,
        errorHandler: () => Promise.reject(new Error('Noticed output file is not writable.')),
      },
    ],
  },
};

module.exports = {
  scramblerActions,
  streamConfigs,
};
