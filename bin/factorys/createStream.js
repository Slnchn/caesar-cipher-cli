const fs = require('fs');
const util = require('util');

const access = util.promisify(fs.access);

/**
 *
 * @param {string} filePath
 * @param {Object} streamConfig
 * @param {ReadStream | WriteStream} streamConfig.std - standart stream to use, if file wasn't specified
 * @param {Function} streamConfig.creator - creator of a new stream object (read or write)
 * @param {Object?} streamConfig.options - stream params
 * @param {Array<{mode: number, errorHandler: Function}>} streamConfig.fileAccessChecks - configs, defining how to check access to specified file and handle errors
 */
module.exports = (filePath, streamConfig) => {
  if (filePath === undefined) {
    return Promise.resolve(streamConfig.std);
  }

  return Promise.all(
    streamConfig.fileAccessChecks.map(({ mode, errorHandler }) =>
      access(filePath, mode).catch(errorHandler)
    )
  ).then(() => streamConfig.creator(filePath, streamConfig.options));
};
