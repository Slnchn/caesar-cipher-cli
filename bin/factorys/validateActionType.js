const { scramblerActions } = require('../constants');

module.exports = (actionType) => {
  switch (true) {
    case actionType === undefined: {
      return Promise.reject(new Error(`Action type wasn't passed.`));
    }
    case !Object.values(scramblerActions).includes(actionType): {
      return Promise.reject(new Error(`There are only 'encode' and 'decode' actions yet !`));
    }
    default: {
      return Promise.resolve(actionType);
    }
  }
};
