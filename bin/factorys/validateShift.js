module.exports = (shift) => {
  const numericShift = Number(shift);
  switch (true) {
    case shift === undefined: {
      return Promise.reject(new Error(`Shift wasn't passed.`));
    }
    case Number.isNaN(numericShift): {
      return Promise.reject(new Error(`Shift should be numeric.`));
    }
    case !Number.isInteger(numericShift): {
      return Promise.reject(new Error(`Shift should be integer.`));
    }
    default: {
      return Promise.resolve(numericShift);
    }
  }
};
