#! /usr/bin/env node

const { pipeline } = require('stream');

const { Command } = require('commander');

// stream transformators
const CEncoder = require('./stream-transformators/CEncoder');
const CNextLiner = require('./stream-transformators/CNextLiner');

// factorys
const createStream = require('./factorys/createStream');
const validateShift = require('./factorys/validateShift');
const validateActionType = require('./factorys/validateActionType');

const { streamConfigs } = require('./constants');

const program = new Command();

program.storeOptionsAsProperties(false).passCommandToAction(false);

program
  .option('-s, --shift <number>', 'cipher shift')
  .option('-i, --input <path>', 'input file path')
  .option('-o, --output <path>', 'output file path')
  .option('-a, --action <type>', 'encode / decode');

program.parse(process.argv);

const options = program.opts();

Promise.all([
  createStream(options.input, streamConfigs.READ_STREAM),
  validateShift(options.shift),
  validateActionType(options.action),
  createStream(options.output, streamConfigs.WRITE_STREAM),
])
  .then(([readStream, shift, action, writeStream]) => {
    pipeline(readStream, new CEncoder(shift, action), new CNextLiner(), writeStream, () => {});
  })
  .catch((err) => {
    process.stderr.write(err.message);
    process.exit(-1);
  });
