# caesar-cipher-cli

## Enabling CLI

1. Create an empty folder on your computer;
2. Open a command line interpreter inside the folder you just created;
3. Clone my repo inside this folder via `git clone git@gitlab.com:Slnchn/caesar-cipher-cli.git .`;
4. Install project dependencies via `npm i`;
5. Install project as global module via `npm i . -g`.

Now you can use this app globally in your system using `caesar-cipher` keyword. Hooray!

## Usage

### Parameters

```
-s, --shift <integer> - the number of positions by which all letters of the original text will be shifted. Required;

-a, --action <('encode'|'decode')> - the action to be performed on the initial text. Required;

-i, --input <string?> - path to the file with initial text. If not specified, stdin will be used;

-o, --output <string?> - path to the file, where the result will be written. If not specified, stdout will be used;
```

### Example

`caesar-cipher -i ./input.txt -s 2 -a encode`
